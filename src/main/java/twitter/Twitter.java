package twitter;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Twitter {
    private List<User> users;
    private User activeUser;
    private boolean isLogin;

    public User getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(User activeUser) {
        this.activeUser = activeUser;
    }

    //constructor
    public Twitter() {
        users=new ArrayList<>();
        isLogin=false;
        activeUser=null;
    }

    public List<User> getUsers() {
        return users;
    }

    public void addUsers(User user) {
       this.users.add(user);
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    /**
     * this function gets a username and a password and tries to create an account with given details
     * @param userName input username
     * @param password input password
     * @throws Exception if an account with given details Already exist this function throws an exception
     */
    public void SignUp(String userName,String password) throws Exception{
        for(User user:this.users){
            if(user.getUserName().equals(userName)){
                throw new Exception("An account with the same username is available");
            }
        }
        User newUser=new User(userName,password);
        addUsers(newUser);
    }

    /**
     * this function gets a username and search it in users list
     * @param username input username
     * @return Requested user
     * @throws Exception This function creates an Exception if the account does not exist with the given detail
     */
    public User searchUser(String username)throws Exception{
        for(User user:this.users){
            if(user.getUserName().equals(username)){
                return user;
            }
        }
        throw new Exception("There is no such account");
    }

    /**
     * This function receives the username and password and logs in to the account if possible
     * @param username input username
     * @param password input password
     * @throws Exception This function creates an Exception if the account does not exist with the given details
     */
    public void login(String username ,String password)throws Exception{
        for(User user:this.users){
            if(user.getUserName().equals(username)){
                if(user.getPassword().equals(password)){
                    this.activeUser=user;
                    this.isLogin=true;
                    return;
                }else{
                    throw new Exception("incorrect password");
                }
            }
        }
        throw new Exception("There is no such account");
    }

    /**
     * This function performs the operation of logging out of the account
     */
    public void logout(){
        if(this.isLogin){
            this.activeUser=null;
            this.isLogin=false;
        }
    }

    /**
     * this function prints Introductory message
     */
    public void welcomeMessage(){
        String str="|||||||||||||||||||||||||||||||\n" +
                "|||||||||||||||||||||||||||||||\n" +
                "||||||||||||/      \\|||||||||||\n" +
                "|||||||||||/        <||||||||||\n" +
                "|||||||||||/       ||||||||||||\n" +
                "||||/               /||||||||||\n" +
                "|||||/              /||||||||||\n" +
                "|||||||/           /|||||||||||\n" +
                "||||||||||/      //||||||||||||\n" +
                "|||||||||/     //||||||||||||||\n" +
                "|||||||/     //||||||||||||||||\n" +
                "|||||||||||||||||||||||||||||||\n" +
                "|||||||||||||||||||||||||||||||\n"+
                "############################### \n" +
                "#     CONSOLE TWITTER         # \n" +
                "############################### \n";

        System.out.println(str);
    }

    /**
     * this function prints menu list
     */
    public void menu(){
        String str=" ───────────────────────\n" +
                "                        \n" +
                "                        \n" +
                "         Sign up        \n" +
                "                        \n" +
                "          Login         \n" +
                "                        \n" +
                "          about         \n" +
                "                        \n" +
                "          Quit          \n" +
                "                        \n" +
                "                        \n" +
                "  © 2021 by Erfan Karami\n" +
                " ───────────────────────";
        System.out.println(str);
    }

    /**
     * this function prints some new line characters"
     */
    public void cls(){
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    /**
     * this function prints all of Allowed commands for a user after login
     */
    public void userCommandsList(){
        String str=" ───────────────────────\n" +
                "         Logout         \n" +
                "       My profile       \n" +
                "         Tweet          \n" +
                "         Follow         \n" +
                "        Unfollow        \n" +
                "        Followers       \n" +
                "        Following       \n" +
                "        Timeline        \n" +
                "        Profile         \n" +
                "          Like          \n" +
                "         Unlike         \n" +
                " ───────────────────────";
        System.out.println(str);
    }

    /**
     * this function make a delay in running the project
     * @param seconds Delay rate in seconds
     */
    public void delay(int seconds){
        try{
            TimeUnit.SECONDS.sleep(seconds);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    /**
     * this function prints a message about this project
     */
    public void aboutMessage(){
        String str="|||||||||||||||||||||||||||||||\n" +
                "|||||||||||||||||||||||||||||||\n" +
                "||||||||||||/      \\|||||||||||\n" +
                "|||||||||||/        <||||||||||\n" +
                "|||||||||||/       ||||||||||||\n" +
                "||||/               /||||||||||\n" +
                "|||||/              /||||||||||\n" +
                "|||||||/           /|||||||||||\n" +
                "||||||||||/      //||||||||||||\n" +
                "|||||||||/     //||||||||||||||\n" +
                "|||||||/     //||||||||||||||||\n" +
                "|||||||||||||||||||||||||||||||\n" +
                "|||||||||||||||||||||||||||||||\n"+
                "############################### \n" +
                "#     CONSOLE TWITTER         # \n" +
                "############################### \n" +
                "This program as an          \n" +
                "advanced programming project\n" +
                "in the spring of 1400       \n" +
                "Implemented by Erfan Karami.";
        System.out.println(str);
    }
}
