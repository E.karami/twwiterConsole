package twitter;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class User {
    private String userName;
    private String password;
    private List<User> followers;
    private List<User> followings;
    private List<Tweet> tweets;
    private List<Tweet> likedTweet;


    //constructor

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
        this.followers=new ArrayList<>();
        this.followings=new ArrayList<>();
        this.tweets=new ArrayList<>();
        this.likedTweet=new ArrayList<>();
    }

    //getter and setters


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void printFollowerList() {
        StringBuilder stringBuilder=new StringBuilder();
        for (User follower : this.followers) {
            stringBuilder.append(follower.getUserName());
            stringBuilder.append("\n");
        }
        System.out.print(stringBuilder.toString());
    }

    public void addFollower(User follower) {
        this.followers.add(follower);
    }

    /**
     * this function prints Following list of the user
     */
    public void printFollowingList(){
        StringBuilder stringBuilder=new StringBuilder();
        for (User following : this.followings) {
            stringBuilder.append(following.getUserName());
            stringBuilder.append("\n");
        }
        System.out.print(stringBuilder.toString());
    }

    /**
     * this function gets an User and tries to follow it
     * @param following input user
     * @throws Exception throws an exception if Has followed the user before or input user and this user be similar
     */
    public void Follow(User following)throws Exception{
        if(this.getUserName().equals(following.getUserName())){
            throw new Exception("You cant follow yourself");
        }
        for(User user:this.followings){
            if(user.getUserName().equals(following.getUserName())){
                throw new Exception("You have already followed this user");
            }
        }
        this.followings.add(following);
        following.followers.add(this);
    }

    /**
     * this function gets an User and tries to unfollow it
     * @param user input user
     * @throws Exception throws an exception if Has not followed the user before
     */
    public void unFollow(User user)throws Exception{
        for(User u:this.followings){
            if(u.getUserName().equals(user.getUserName())){
                this.followings.remove(user);
                user.followers.remove(this);
                return;
            }
        }
        throw new Exception("A user with this username was not found in your following list");
    }

    /**
     *this function gets an tweet ID and searach it in liked tweet list
     * @param tweetID tweet Id
     * @return
     * @throws Exception throws exception if there is no any tweet in liked tweet lists by given details
     */
    public Tweet getLikedTweet(String tweetID) throws Exception{
        for(Tweet tweet:this.likedTweet){
            if(tweet.getTweetID().equals(tweetID)){
                return tweet;
            }
        }
        throw new Exception("No tweets were found with this ID");
    }

    public List<Tweet> getTweets() {
        return tweets;
    }

    /**
     *  this function gets a tweet and tries to like it
     * @param tweet input tweet
     * @throws Exception throws an exception if tweet already liked  by this user
     */
    public void like(Tweet tweet) throws Exception{
        for (Tweet value : this.likedTweet) {
            if (value.getTweetID().equals(tweet.getTweetID())) {
                throw new Exception("You have already liked this tweet");
            }
        }
        this.likedTweet.add(tweet);
        tweet.increaseLikes();
        tweet.setLikedBy(this.userName);
    }

    /**
     * this function gets a tweet and tries to unlike it
     * @param tweet input tweet
     * @throws Exception throws an exception if tweet already Not liked  by this user
     */
    public void unLike(Tweet tweet)throws Exception{
        for(int i=0;i<this.likedTweet.size();i++){
            if(this.likedTweet.get(i).getTweetID().equals(tweet.getTweetID())){
                this.likedTweet.remove(i);
                tweet.decreaseLikes();
                tweet.deleteLikedBy(this.userName);
                return;
            }
        }
        throw new Exception("You have not liked this tweet before");
    }

    /**
     * this function prints user profile
     */
    public void myProfile(){
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("Username : ").append(this.userName);
        stringBuilder.append("\n\n");
        for (Tweet tweet : this.tweets) {
            stringBuilder.append(tweet.toString()).append("\n");
        }
        System.out.println(stringBuilder.toString());
    }

    /**
     * this function gets an content and tries to add a tweet to the tweet list of this user
     * @param content input content
     */
    public void addTweet(String content){
        String tweetId=this.userName+"@"+this.tweets.size();
        Tweet tweet=null;
        try {
            tweet=new Tweet(tweetId,content);
        }catch(Exception ex){
            System.err.println(ex.getMessage());
        }

        this.tweets.add(tweet);
        System.out.println("Successful operation...");
    }

    /**
     * this function gets an tweet id and searches it in list of user tweets
     * @param tweetID input tweetID
     * @return founded tweet
     * @throws Exception throws an exception if there is no any tweet with given ID in list of user tweets
     */
    public Tweet searchTweet(String tweetID)throws Exception{
        for(Tweet tweet:this.tweets){
            if(tweet.getTweetID().equals(tweetID)){
                return tweet;
            }
        }
        throw new Exception("No tweets were found with this ID");
    }

    /**
     * this function prints timeline of a user
     */
    public void timeLine(){
        List<Tweet> timeLineTweets = new ArrayList<>(this.tweets);
        for(User newUser:this.followings){
            timeLineTweets.addAll(newUser.getTweets());
        }
        timeLineTweets.sort(Tweet.tweetComparator);
        StringBuilder stringBuilder=new StringBuilder();
        Pattern pattern=Pattern.compile("(.*@)");
        for(Tweet tweet:timeLineTweets){
            Matcher matcher=pattern.matcher(tweet.getTweetID());
            String user=null;
            if(matcher.find()){
                user=matcher.group();
            }
            assert user != null;
            user=user.substring(0,user.length()-1);
            stringBuilder.append(user).append(":").append("\n");
            stringBuilder.append(tweet);
            stringBuilder.append("\n");
        }
        System.out.println(stringBuilder.toString());
    }



}
