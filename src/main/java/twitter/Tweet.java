package twitter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * class for tweets
 */
public class Tweet{
    private final String TweetID;
    private final String content;
    private int likes;
    private final Date tweetDate;
    private List<String> likedBy;


//constructors

    public Tweet(String tweetID, String content) throws Exception {
        if(content.length()>140){
            throw new Exception("tweet size out of Permitted size");
        }
        this.TweetID = tweetID;
        this.content = content;
        this.likes=0;
        this.tweetDate=new Date();
        this.likedBy=new ArrayList<>();
    }

    public String getTweetID() {
        return TweetID;
    }

    public String getContent() {
        return content;
    }

    public List<String> getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(String str) {
        this.likedBy.add(str);
    }

    public void deleteLikedBy(String str){
        this.likedBy.remove(str);
    }

    public int getLikes() {
        return likes;
    }

    public void increaseLikes() {
        this.likes++;
    }

    public void decreaseLikes() {
        this.likes--;
    }

    public Date getTweetDate() {
        return tweetDate;
    }

    @Override
    public String toString() {
        String[] strings=this.content.split("##");
        StringBuilder stringBuilder=new StringBuilder();
        for(String str:strings){
            stringBuilder.append(str);
            stringBuilder.append("\n");
        }
        stringBuilder.append("\n");
        stringBuilder.append(" ❤ ").append(this.likes).append("          id: ").append(this.TweetID).append("\n");
        stringBuilder.append(" _______________________________");

        return stringBuilder.toString();
    }

    /**
     * this comparator used by ArrayList.sort() to sort elements
     */
    public static Comparator<Tweet> tweetComparator=new Comparator<Tweet>() {
        @Override
        public int compare(Tweet o1, Tweet o2) {
            if(o1.getTweetDate().after(o2.getTweetDate())){
                return 1;
            }
            if(o1.getTweetDate().before(o2.getTweetDate())){
                return -1;
            }
            return 0;
        }
    };
}
