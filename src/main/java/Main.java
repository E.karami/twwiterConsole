import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import twitter.*;
public class Main {
    public static void main(String[] args){
        Scanner in=new Scanner(System.in);
        Twitter twitter=new Twitter();
        twitter.cls();
        twitter.welcomeMessage();
        twitter.delay(5);
        twitter.cls();
        while (true){
            twitter.menu();
            String str=in.nextLine();
            switch (str){
                case "Sign up":{
                    twitter.cls();
                    String username;
                    String password;
                    System.out.println("Please enter username and password");
                    System.out.print("username: ");
                    username=in.nextLine();
                    while (username.equals("")){
                        System.out.println("username cant be empty!");
                        System.out.print("username: ");
                        username=in.nextLine();
                    }
                    System.out.print("password: ");
                    password=in.nextLine();
                    while (password.equals("")){
                        System.out.println("password cant be empty!");
                        System.out.print("password: ");
                        password=in.nextLine();
                    }
                    try{
                        twitter.SignUp(username,password);
                        System.out.println("Successful operation...");
                        twitter.delay(2);
                    }catch (Exception ex){
                        System.err.println(ex.getMessage());
                        twitter.delay(2);
                    }
                }
                break;
                case "Login":{
                    twitter.cls();
                    String username;
                    String password;
                    System.out.println("Please enter username and password");
                    System.out.print("username: ");
                    username=in.nextLine();
                    System.out.print("password: ");
                    password=in.nextLine();
                    try{
                        twitter.login(username,password);
                        System.out.println("Successful operation...");
                        twitter.delay(2);
                    }catch (Exception exception){
                        System.err.println(exception.getMessage());
                        twitter.delay(2);
                        break;
                    }
                    User active=twitter.getActiveUser();
                    boolean flag=true;
                    while (flag){
                        twitter.cls();
                        twitter.userCommandsList();
                        String string=in.nextLine();
                        twitter.cls();
                        switch (string){
                            case "Logout":{
                                twitter.logout();
                                flag=false;
                            }
                            break;
                            case "My profile":{
                                active.myProfile();
                                System.out.print("\npress a key to back to command list:");
                                in.nextLine();
                            }
                            break;
                            case "Tweet":{
                                System.out.println("Enter text with less than 140 characters Enter \"##\" to go to the new line.");
                                String content=in.nextLine();
                                active.addTweet(content);
                                twitter.delay(4);
                            }
                            break;
                            case "Follow":{
                                System.out.print("Enter the username you want to follow: ");
                                String userName=in.nextLine();
                                User iWantToFollow=null;
                                try{
                                    iWantToFollow=twitter.searchUser(userName);
                                    active.Follow(iWantToFollow);
                                    System.out.println("Successful operation...");
                                    twitter.delay(2);
                                }catch(Exception ex){
                                    System.err.println(ex.getMessage());
                                    twitter.delay(2);
                                }
                            }
                            break;
                            case "Unfollow":{
                                System.out.print("Enter the username you want to unfollow: ");
                                String userName=in.nextLine();
                                User iWantToUnFollow=null;
                                try{
                                    iWantToUnFollow=twitter.searchUser(userName);
                                    active.unFollow(iWantToUnFollow);
                                    System.out.println("Successful operation...");
                                    twitter.delay(2);
                                }catch(Exception ex){
                                    System.err.println(ex.getMessage());
                                    twitter.delay(2);
                                }
                            }
                            break;
                            case "Followers":{
                                active.printFollowerList();
                                System.out.print("\npress a key to back to command list:");
                                in.nextLine();
                            }
                            break;
                            case "Following":{
                                active.printFollowingList();
                                System.out.print("\npress a key to back to command list:");
                                in.nextLine();
                            }
                            break;
                            case "Timeline":{
                                active.timeLine();
                                System.out.print("\npress a key to back to command list:");
                                in.nextLine();
                            }
                            break;
                            case "Profile":{
                                System.out.print("Enter the desired username :");
                                String userName=in.nextLine();
                                try{
                                    twitter.searchUser(userName).myProfile();
                                    System.out.print("\npress a key to back to command list:");
                                    in.nextLine();

                                }catch(Exception ex){
                                    System.err.println(ex.getMessage());
                                    twitter.delay(2);
                                }
                            }
                            break;
                            case "Like":{
                                System.out.print("Enter the desired tweet ID :");
                                String tweetID=in.nextLine();
                                Pattern pattern=Pattern.compile("(.*@)");
                                Matcher matcher=pattern.matcher(tweetID);
                                String user=null;
                                if(matcher.find()){
                                    user=matcher.group();
                                }
                                try{
                                    assert user != null;
                                    user=user.substring(0,user.length()-1);
                                    User likedUser=twitter.searchUser(user);
                                    Tweet tweet=likedUser.searchTweet(tweetID);
                                    active.like(tweet);
                                    System.out.println("Successful operation...");
                                    twitter.delay(2);
                                }catch(Exception ex){
                                    System.err.println(ex.getMessage());
                                    twitter.delay(2);
                                }

                            }
                            break;
                            case "Unlike":{
                                System.out.print("Enter the desired tweet ID :");
                                String tweetID=in.nextLine();
                                try{
                                    Tweet tweet=active.getLikedTweet(tweetID);
                                    active.unLike(tweet);
                                    System.out.println("Successful operation...");
                                    twitter.delay(2);
                                }catch (Exception ex){
                                    System.err.println(ex.getMessage());
                                    twitter.delay(2);
                                }
                            }
                            break;
                            default:{
                                System.out.println("incorrect command please try again!");
                                twitter.delay(2);
                            }
                        }
                    }
                }
                break;
                case "about":{
                    twitter.cls();
                    twitter.aboutMessage();
                    twitter.delay(10);
                }
                break;
                case "Quit":{
                    return;
                }
                default:{
                    System.out.println("incorrect command please try again!");
                    twitter.delay(2);
                }
            }
            twitter.cls();
        }
    }
}
